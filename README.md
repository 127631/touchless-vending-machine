# Touchless Vending Machine using LeapMotion
------

The goal of this project is to adapt vending machines so they can be used without even having to touch them. The main idea is that this solution can be applied to existing vending machines so they can be operative in a really short period of time.  

### Links
* Repository with source code: https://bitbucket.org/127631/touchless-vending-machine/src/master/
* Link to demo video: https://youtu.be/uuHf04ZMkFI  

### Project description
Vending machines are literally everywhere, and they will probably significantly decrease their earnings if nothing is done to adapt them to the post-covid era. Furthermore, people who still use them will have a notable risk of contagion if they’re not careful while doing so.
That’s why it’s really important to turn vending machines into touchless ones, which can be done just by using an affordable device like LeapMotion and a relatively simple computer program.  
The main goal of this solution is that it can easily be applied to different kinds of existing vending machines since it’s way more affordable (it doesn’t require the huge inversion of replacing every vending machine) and designing new ones implies a lot of work which can’t be done in such a short period of time.  

### Notes
Please note that the project is designed to be used with a physical vending machine. The blocks displayed on the screen are just used to give a quick graphical representation of the products and to detect where the finger is pointing at.  
A connection to the vending machine must be made using a controller/driver with an API. Once the API is connected, instructions like `Debug.Log("vendingMachineController.lightProduct(nProd,Color)");` or `Debug.Log("vendingMachineController.pickProduct(nProd);` must be replaced with calls to the corresponding API functions.  

### Configuration
* Input the codes each product has currently on the vending machine, with the desired number of rows and columns (each row may have a different number of columns), in CSV format.  
* The program is ready to connect it to an API which interacts directly with the vending machine. It just needs two functions: one to enlighten a product given its code and another one to select it.  

------
Project by Jorge Bruned (email: bruned.127631`AT`e.unavarra.es)