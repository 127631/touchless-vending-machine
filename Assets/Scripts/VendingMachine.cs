﻿using Leap;
using Leap.Unity.Examples;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class VendingMachine : MonoBehaviour
{
    public TextAsset config;
    public TextAsset prices;
    public GameObject productPrefab;
    public GameObject UI;
    string businessName = "", currency = "";
    const float MIN_X = -1.5f, MAX_X = 1.5f, MIN_Y = -5, MAX_Y = 1.8f;
    public const int WAITING_TIME = 6; // in seconds
    private int pickedProd = -1;
    private float pickedTime = -1;
    const int MAX_PRODUCTS = 100;
    private Product[] products = new Product[MAX_PRODUCTS];

    // Start is called before the first frame update
    void Start()
    {
        string[] lines = config.text.Split(System.Environment.NewLine.ToCharArray());
        foreach(string line in lines)
        {
            string newLine = line.Replace("businessName = ","");
            if (!newLine.Equals(line))
                businessName = line.Trim();
            else if (!(newLine = line.Replace("currency = ", "")).Equals(line))
                currency = line.Trim();
        }
        if (businessName.Length == 0 || currency.Length == 0)
        {
            Debug.LogError("Error! config.txt is incomplete");
            Debug.Break();
            Application.Quit();
        }
        transform.localScale = new Vector3(MAX_X - MIN_X, MAX_Y - MIN_Y, 0.1f);
        transform.localPosition = new Vector3(MIN_X, (MAX_Y + MIN_Y) / (float)2, 2.5f);
        lines = prices.text.Split(System.Environment.NewLine.ToCharArray());
        Array.Reverse(lines);
        int nProducts = 0;
        float height = /*(MAX_Y - MIN_Y)*/1 / (float)lines.Length;
        int nRow = 0;
        foreach(string line in lines)
        {
            if (line.StartsWith("#") || line.Length == 0)
                continue;
            string[] productLine = line.Split(',');
            float width = /*(MAX_X - MIN_X)*/1 / (float)productLine.Length;
            int nCol = 0;
            foreach (string product in productLine)
            {
                GameObject newProd = GameObject.Instantiate(productPrefab, transform, true);
                newProd.transform.localScale = new Vector3(width,height,1);
                newProd.transform.localPosition = new Vector3(nCol * width + newProd.transform.localScale.x * 0.5f, nRow * height + newProd.transform.localScale.y * 0.5f, 1);
                newProd.GetComponent<Product>().vm = GetComponent<VendingMachine>();
                newProd.GetComponent<Product>().code = product;
                newProd.name = ""+nProducts;
                newProd.SetActive(true);
                nCol++;
                if (++nProducts > MAX_PRODUCTS)
                {
                    Debug.LogError("Error! There are more products than supported (" + MAX_PRODUCTS + ")");
                    Debug.Break();
                    Application.Quit();
                }
                else
                    products[nProducts - 1] = newProd.GetComponent<Product>();
            }
            nRow++;
        }
        if (nProducts == 0)
        {
            Debug.LogError("Error! products.csv is empty");
            Debug.Break();
            Application.Quit();
        }
    }

    public void pickProduct(int nProd)
    {
        Debug.Log("[API CALL] vendingMachineController.setLightColor(" + nProd + ",Color.green)");
        pickedProd = nProd;
        pickedTime = Time.time;
        for (int i = 0; i < products.Length; i++)
            if (products[i] == null)
                break;
            else if (products[i].name != "" + nProd)
                products[i].GetComponent<Renderer>().material.SetColor("_Color", Color.white);
    }

    public void hoverProduct(int nProd)
    {
        Debug.Log("[API CALL] vendingMachineController.setLightColor(" + nProd + ",Color.white)");
    }

    public void unhoverProduct(int nProd)
    {
        Debug.Log("[API CALL] vendingMachineController.setLightColor(" + nProd + ",null)");
    }

    public void giveProduct(int nProd)
    {
        Debug.Log("[API CALL] vendingMachineController.pickProduct(" + nProd + ")");
    }

    public void Update()
    {
        if (pickedProd >= 0 && WAITING_TIME > Time.time - pickedTime)
        {
            UI.SetActive(true);
            UI.transform.Find("TimeLeft").GetComponent<Text>().text = "Product number " + products[pickedProd].code + " will come out in " + (int)(WAITING_TIME - Time.time + pickedTime + 1) + " seconds";
        }
        else if (pickedProd >= 0)
        {
            UI.SetActive(false);
            giveProduct(pickedProd);
            pickedProd = -1;
            pickedTime = -1;
        }
    }

}
