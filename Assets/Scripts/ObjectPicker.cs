﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPicker : MonoBehaviour
{
    private GameObject bone1, bone2, bone3;

    // Start is called before the first frame update
    void Start()
    {
        bone1 = transform.Find("index").Find("bone1").gameObject;
        bone2 = transform.Find("index").Find("bone2").gameObject;
        bone3 = transform.Find("index").Find("bone3").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        Pointable obj;
        // Does the ray intersect any objects excluding the player layer
        //Debug.Log(Vector3.Angle(bone3.transform.position - bone2.transform.position, bone2.transform.position - bone1.transform.position));
        if (Vector3.Angle(bone3.transform.position - bone2.transform.position, bone2.transform.position - bone1.transform.position) > 45)
            return;
        else if (Physics.Raycast(bone3.transform.position, (bone3.transform.position - bone2.transform.position), out hit) && (obj = hit.transform.GetComponent<Pointable>()) != null)
            obj.Pointed(Vector3.Distance(hit.transform.position, bone3.transform.position)/*, Vector3.Angle(hit.normal, bone3.transform.position - bone2.transform.position)*/);
        else if (hit.transform != null)
            Debug.Log("Didn't hover " + hit.transform.name);
    }
}
