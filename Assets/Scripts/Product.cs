﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Product : Pointable
{
    [HideInInspector]
    public VendingMachine vm;
    public string code;

    static readonly Color NORMAL = Color.white, HOVER = Color.yellow, PICKED = Color.red;

    public override void OnHover()
    {
        //Debug.Log("Hover");
        if (GetComponent<Renderer>().material.GetColor("_Color") != PICKED)
            GetComponent<Renderer>().material.SetColor("_Color", HOVER);
        vm.hoverProduct(Int32.Parse(transform.name.Replace("Product", "")));
    }
    
    public override void OnUnHover()
    {
        //Debug.Log("UnHover");
        if (GetComponent<Renderer>().material.GetColor("_Color") != PICKED)
            GetComponent<Renderer>().material.SetColor("_Color", NORMAL);
        vm.unhoverProduct(Int32.Parse(transform.name));
    }
    
    public override void OnClick()
    {
        //Debug.Log("Picked " + gameObject.name);
        StartCoroutine("clickAction");
        vm.pickProduct(Int32.Parse(transform.name.Replace("Product", "")));
        //GetComponent<Renderer>().material.SetColor("_Color", Color.red);
    }

    public IEnumerator clickAction()
    {
        GetComponent<Renderer>().material.SetColor("_Color", PICKED);
        yield return new WaitForSeconds(VendingMachine.WAITING_TIME);
        GetComponent<Renderer>().material.SetColor("_Color", hovered ? HOVER : NORMAL);
    }

}
