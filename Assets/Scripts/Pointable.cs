﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public abstract class Pointable : MonoBehaviour
{
    const float CLICK_THRESHOLD = 0.03f;
    const float HOVER_DURATION = 0.05f; // in seconds
    const float TIME_BETWEEN_CLICKS = 0.5f; // in seconds
    //const int ANGLE_THRESHOLD = 3; // in degrees
    private float distanceFirstPointed = 0, /*angleFirstPointed = 0, */lastHovered = 0, lastClicked = 0;
    public bool hovered = false, justClicked = false;

    public void Pointed(float distance/*, float angle*/)
    {
        if (distanceFirstPointed == 0/* || angleFirstPointed + ANGLE_THRESHOLD < angle || angleFirstPointed - ANGLE_THRESHOLD > angle*/)
        {
            distanceFirstPointed = distance;
            //angleFirstPointed = angle;
            Hover();
        }
        else if (distance >= distanceFirstPointed - CLICK_THRESHOLD)
        {
            Hover();
            if (distance > distanceFirstPointed)
                distanceFirstPointed = distance;
        }
        else if (!justClicked)// if (lastClicked + TIME_BETWEEN_CLICKS < lastHovered)
            Click();
        lastHovered = Time.time;
    }

    public void Hover()
    {
        if (!hovered)
        {
            OnHover();
            justClicked = false;
        }
        hovered = true;
        lastHovered = Time.time;
    }

    public void UnHover()
    {
        if (hovered)
            OnUnHover();
        hovered = false;
        justClicked = false;
        distanceFirstPointed = 0;
        //angleFirstPointed = 0;
    }

    public void Click()
    {
        lastClicked = Time.time;
        justClicked = true;
        OnClick();
    }

    public abstract void OnHover();
    public abstract void OnUnHover();
    public abstract void OnClick();

    public void Update()
    {
        if (lastHovered + HOVER_DURATION < Time.time && hovered)
            UnHover();
        else if (lastClicked + TIME_BETWEEN_CLICKS < Time.time && justClicked)
            justClicked = false;
    }
}